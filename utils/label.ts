import { Locator } from "@playwright/test";

export class Label{
    static async getLabel(element: Locator): Promise<string | null>{
        let label: string | undefined = (await element.innerText()).trim();
        if(label)
            return label;
    
        label = (await element.getAttribute('aria-label'))?.trim();
        if(label)
            return label;
    
        label = (await element.getAttribute('title'))?.trim();
        if(label)
            return label;
    
        return null;
    }
}