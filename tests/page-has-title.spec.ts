import { expect, test } from '@playwright/test';
import { URLS } from '../fixtures/url.fixture';

test('Root url has title', async ({ page }) => {
  await page.goto('/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle('Sample Page'); //Favoriser le libellé exacte pour la non régression
});

test('All URLs have a title', async ({ page }) => {
  for(const url of URLS){
    await page.goto(url.url);
  
    await expect(page).toHaveTitle(url.title);
    console.log(await page.title());
  }
});