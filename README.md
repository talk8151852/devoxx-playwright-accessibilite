# Comment maintenir l'accessibilité d'un site avec Playwright ?

## Disclaimer

Je vais beaucoup parler d'accessibilité visuelle, mais je ne réduis pas l'accessibilité uniquement à cela !

Je fais du .NET et des technologies Microsoft, désolé :/

## Qui suis-je ?

Alexandre Ruiz aka Lelexxx, Tech Lead .NET chez Apside depuis trois ans, j'encadre des équipes dans la conception et le développement de solutions logicielles (application web, API).

En parallèle, en tant qu'enseignant en JavaScript à l'IUT informatique de Clermont-Ferrand, je partage mes connaissances pour former les étudiants aux meilleures pratiques du développement web.

Vous pouvez me retrouver sur mes différents réseaux sociaux :

- https://twitter.com/l_lexxx
- https://github.com/lelexxx
- https://gitlab.com/rruiz.alex

## L'accessibilité dans le web

Aujourd'hui, plus de 10% de la population française est atteinte d'une déficience visuelle alors que dans le même temps, seulement 10% du web est accessible. Mais comment peut-on faire pour améliorer cela ? Comment assurer l’accessibilité de votre application tout au long de son développement ?

Regardons ensemble comment Playwright, le nouvel outil des tests de bout en bout, vous permettra de vous engager dans le développement d’une nouvelle génération d’applications accessibles.

## Qu'est-ce que la norme RGAA ?

Le référentiel général d’amélioration de l’accessibilité (RGAA) définit les règles permettant de garantir l'amélioration de l'accessibilité au travers du cadre légal et du cadre technique.
C'est sur ce dernier que nous allons nous reposer pour définir les bonnes pratiques de développement de l'accessibilité et mettre au point nos tests.

L'intégralité de la spécificité technique est disponible ici : https://accessibilite.numerique.gouv.fr/methode/criteres-et-tests/

## Back to basic : HTML

Dans l'accessibilité, pas besoin de Framework ou de nouvelles pratiques pour faire les choses bien. On retourne aux bases d'une page web avec le langage HTML.
Le langage HTML doit être considéré comme la grammaire du web, c'est-à-dire que chaque élément va porter un sens concret, et c'est sur ce sens que l'on doit se reposer pour définir l'accessibilité d'une page web.

## Playwright, notre framework de test !

Playwright est un Framework de test de bout en bout (`end-to-end` ou `e2e`) supportant différents navigateurs et multi-langages (TypeScript, JavaScript, Python, .NET, Java).

## Initialisation du projet

Playwright s'installe au sein d'un projet (front) existant ou dans un projet vierge via `npm` et la commande `npm init playwright@latest` ou `npm install playwright@latest` si le projet est déjà initilisé.

Après quelques configurations (choix du navigateur, de la résolution, de l'URL à tester...) nous sommes prêts pour coder nos premiers tests !

Vous pouvez retrouver l'installation détaillée de Playwright ici : https://playwright.dev/docs/intro

## Comment dynamiser et massifier nos tests ?

Par défaut, un test va s'exécuter sur une page web. Il est donc judicieux de vouloir les exécuter sur plusieurs pages (déterminé par importance, par Template de page...).
Afin de variabiliser les pages web et pouvoir en tester un grand nombre, tout en gardant notre code maintenable et réutilisable, nous allons utiliser les fixtures.

## On implémente nos premiers tests

Afin de garantir l'accessibilité d'un site, un certain nombre de règles sont à vérifier.
Pour commencer, nous allons implémenter les tests suivants :

- Vérifier que la page possède une balise `titre`
- Vérifier que chaque image possède un attribut `alt`

## Les attributs ARIA

Durant nos développements, nous avons l'occasion de rencontrer des attributs HTML de type aria-*. Mais à quoi correspondent-ils ? Ils sont les gardiens des clés de l'accessibilité dans le web. C'est eux qui permettent d'indiquer aux navigateurs et aux assistants comment un site doit être utilisé (le rôle d'un élément, son label, s'il doit s'afficher ou non Etc.) !

Il est important, pour ne pas dire primordial, d'intégrer ces attributs dans nos tests ! Mettons donc en place les tests suivants :

- Vérifier que chaque champ de formulaire possède un libellé (via `aria-labelledby` ou `aria-label`)
- Masquer les éléments non accessibles ou polluant l'accessibilité via `aria-hidden`
- Récupérer des éléments via son rôle `aria-role` pour pallier les erreurs HTML

## Notre premier parcours utilisateurs

Lorsque l'on parle d'accessibilité, l'une des principales composantes est la navigation via le clavier. Cette pratique est principalement utilisée par les personnes ayant une déficience visuelle. Pour ce faire, l'utilisateur va utiliser la touche tabulation pour accéder aux différents éléments avec lesquels il peut interagir (lien, champ de formulaire).

Nous allons donc vérifier que la barre de recherche de notre application est parfaitement accessible via l'attribut html `role="searchbox"`.

## Et si on intégrait cela dans des pipelines ?

Pour lancer nos tests sans interface graphique (de façon `headless`) nous allons exécuter la commande suivante : `npx playwright test`. Cette commande va exécuter tous les tests que nous avons écrits précédemment sans ouvrir le navigateur, et à la fin des tests un rapport va être généré.

Mais du coup, comment pouvons-nous récupérer ce rapport ? Rien de plus simple, il suffit d'exécuter la commande suivante : `npx playwright show-report`. Un fois le rapport obtenu, il s'affichera et vous aurez alors la possibilité de le consulter et de l'exploiter (l'envoyer par mail, afficher le rapport dans votre pipeline Etc.)

## Conclusion

Vous voilà donc armés pour passer au crible vos applications web et garantir leur accessibilité !
En plus des tests automatisés que vous mettez en place vous pouvez aussi vous équiper d'outils complémentaires qui vous donnerons rapidement un premier état des lieux de votre code.
On pourra citer `lighthouse` par exemple que vous pouvez utiliser via une extension chrome, directement sur leur site ou en utilisant le package JS dédié.

Mais le meilleur moyen de tester reste l'expérience utilisateur. N'hésitez pas à vous rapprocher d’une personne confrontée à l'accessibilité au quotidien, ou utilisez un assistant pour naviguer sur un site.

Merci à tous pour votre présence et votre attention !
